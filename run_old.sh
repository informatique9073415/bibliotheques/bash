#!/bin/bash

function multiselect {
    # little helpers for terminal print control and key input
    ESC=$( printf "\033")
    cursor_blink_on()   { printf "$ESC[?25h"; }
    cursor_blink_off()  { printf "$ESC[?25l"; }
    cursor_to()         { printf "$ESC[$1;${2:-1}H"; }
    print_inactive()    { printf "$2   $1 "; }
    print_active()      { printf "$2  $ESC[7m $1 $ESC[27m"; }
    get_cursor_row()    { IFS=';' read -sdR -p $'\E[6n' ROW COL; echo ${ROW#*[}; }
    print_warning()     { printf "$ESC[38;5;208m$1$ESC[0m"; }


    local return_value=$1
    local -n options=$2
    local -n defaults=$3
    local mono_select=$4
    # Vérifier si le cinquième argument a été fourni
    if [[ -n $5 ]]; then
        local -n exclusiveOptions=$5
    fi

    local selected=()
    local selected_options=()
    
    for ((i=0; i<${#options[@]}; i++)); do
        if [[ ${defaults[i]} = "true" ]]; then
            selected+=("true")
        else
            selected+=("false")
        fi
        printf "\n"
    done

    # determine current screen position for overwriting the options
    local lastrow=`get_cursor_row`
    local startrow=$(($lastrow - ${#options[@]}))

    # ensure cursor and input echoing back on upon a ctrl+c during read -s
    trap "cursor_blink_on; stty echo; printf '\n'; exit" 2
    cursor_blink_off

    key_input() {
        local key
        IFS= read -rsn1 key 2>/dev/null >&2
        if [[ $key = ""      ]]; then echo enter; fi;
        if [[ $key = $'\x20' ]]; then echo space; fi;
        if [[ $key = "k" ]]; then echo up; fi;
        if [[ $key = "j" ]]; then echo down; fi;
        if [[ $key = $'\x1b' ]]; then
            read -rsn2 key
            if [[ $key = [A || $key = k ]]; then echo up;    fi;
            if [[ $key = [B || $key = j ]]; then echo down;  fi;
        fi 
    }

    toggle_option() {
        warning=""
        local option=$1
        
        
        if [[ $mono_select -eq 1 ]]; then
            local i
            # Basculer l'état de l'option sélectionnée.
            if [[ ${selected[option]} == true ]]; then
                selected[option]=false
            else
                # Avant de mettre une option à true, mettre toutes les autres à false.
                for (( i=0; i<${#selected[@]}; i++ )); do
                    selected[$i]=false
                done
                selected[option]=true
            fi
        
        else
            # Vérifier si activer cette option viole les règles d'exclusivité.
            for excl_option in ${exclusiveOptions[$option]}; do
                if [[ ${selected[$excl_option]} == true ]]; then
                    warning="Warning: Option $option cannot be selected with Option $excl_option."
                    #echo $warning
                    return # Retourner immédiatement sans modifier l'état.
                fi
            done

            # Vérifier l'inverse : si une option exclusive à celle-ci est déjà activée.
            for key in "${!exclusiveOptions[@]}"; do
                for value in ${exclusiveOptions[$key]}; do
                    if [[ "$value" == "$option" && ${selected[$key]} == true ]]; then
                        warning="Warning: Option $option cannot be selected with Option $key due to exclusive rules."
                        #echo $warning
                        return # Retourner immédiatement sans modifier l'état.
                    fi
                done
            done

            # Si aucun avertissement, basculer l'état de l'option.
            if [[ ${selected[option]} == true ]]; then
                selected[option]=false
            else
                selected[option]=true
            fi
        fi

    }




    print_options() {
        
        # print options by overwriting the last lines
        local idx=0
        for option in "${options[@]}"; do
            local prefix="[ ]"
            if [[ ${selected[idx]} == true ]]; then
              prefix="[\e[38;5;46m✔\e[0m]"
            fi

            cursor_to $(($startrow + $idx))
            if [ $idx -eq $1 ]; then
                print_active "$option" "$prefix"
            else
                print_inactive "$option" "$prefix"
            fi
            ((idx++))
        done
        cursor_to $(($startrow + $idx))
         
        print_warning "$warning";
        warning=""
    }

    local active=0
    

    while true; do
        print_options $active

        # user key control
        case `key_input` in
            space)  toggle_option $active;;
            enter)  print_options -1; break;;
            up)     ((active--));
                    if [ $active -lt 0 ]; then active=$((${#options[@]} - 1)); fi; echo -en "\r\E[K" ;
                    if [ $mono_select -eq 1 ]; then toggle_option $active; fi;;
            down)   ((active++));
                    if [ $active -ge ${#options[@]} ]; then active=0; fi; echo -en "\r\E[K" ;
                    if [ $mono_select -eq 1 ]; then toggle_option $active; fi;
        esac
        
    done
    
    # cursor position back to normal
    cursor_to $lastrow
    printf "\n"
    cursor_blink_on

    #retourne toutes les options
    #eval $return_value='("${selected[@]}")'
    
   # Tableau pour collecter les options sélectionnées basées sur 'selected'
    local selected_options=()

    # Itérer sur toutes les options pour vérifier si elles sont sélectionnées
    for i in "${!options[@]}"; do
        if [[ ${selected[$i]} == true ]]; then
            # Ajouter l'option au tableau des options sélectionnées
            selected_options+=("${options[$i]}")
        fi
    done
    #séparer les services par une virgule

    # Retourner le tableau des options sélectionnées à la variable spécifiée
    # Ceci suppose que 'eval' est acceptable pour votre cas d'usage
    echo "Selected options: ${selected_options[@]}"
    eval "$return_value=(\"\${selected_options[@]}\")"
}


#my_options=(   "Option 1"  "Option 2"  "Option 3" )
#preselection=( "false"      "false"      "false"    )

#multiselect result my_options preselection

#idx=0
#for option in "${my_options[@]}"; do
#    echo -e "$option\t=> ${result[idx]}"
#    ((idx++))
#done